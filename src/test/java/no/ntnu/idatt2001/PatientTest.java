package no.ntnu.idatt2001;

import no.ntnu.idatt2001.model.Patient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    @Test
    public void cretePatientWithLettersInSocialSecurityNumber() {
        assertThrows(IllegalArgumentException.class, () -> new Patient("This should be a number,",
                "", "", "", ""));
    }

    @Test
    public void createPatientWithWrongDigitsSocialSecurityNumber() {
        assertThrows(IllegalArgumentException.class, () -> new Patient("123123", "",
                "", "",""));
    }
}
