module no.ntnu.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt2001 to javafx.fxml;
    exports no.ntnu.idatt2001;
    exports no.ntnu.idatt2001.model;
}