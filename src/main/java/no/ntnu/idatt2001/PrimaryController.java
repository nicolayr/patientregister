package no.ntnu.idatt2001;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt2001.model.Patient;
import no.ntnu.idatt2001.model.PatientDataReader;
import no.ntnu.idatt2001.model.PatientRegister;

public class PrimaryController implements Initializable {

    private PatientRegister patients;
    private ObservableList<Patient> patientObservableList;

    @FXML
    private Label statusLabel;
    //TODO did now have time to properly implement this label.
    @FXML
    private TableView<Patient> patientTableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> ssnColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        patients = new PatientRegister();

        // Setting up the table
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ssnColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        // Adding data to an observable list. Any changes to the observable list will automatically
        // update the table in the application during runtime.
        patientObservableList = FXCollections.observableArrayList(patients.getPatients());
        patientTableView.setItems(patientObservableList);
    }


    /**
     * Displays the add new patient box.
     * @param actionEvent when pressing the add new patient icon, or the button in the
     *                    in "file" in the menubar.
     */
    @FXML
    private void displayAddPatientBox(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = displayPopup("add-patient.fxml", "Patient Register - Add new patient");
            AddPatientBox box = loader.getController();
            box.setParentController(this);
        } catch (IOException e) {
            statusLabel.setText("Error opening the add patient window");
        }
    }

    /**
     * Displays the edit patient box.
     * @param actionEvent when pressing the edit patient icon, or the button is the
     *                    in "file" in the menubar.
     */
    @FXML
    private void displayEditPatientBox(ActionEvent actionEvent) {
        Patient selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
        if(selectedPatient != null) {
            try {
                // Load window
                FXMLLoader loader = displayPopup("edit-patient.fxml", "Patient Register - Edit patient");
                EditPatientBox box = loader.getController();
                box.setParentController(this);

                // Send information about selected patient
                box.setPatient(selectedPatient);
                box.setTextFields(selectedPatient);
            } catch (IOException e) {
                statusLabel.setText("Error opening the edit patient window");
            }
        } else {
            statusLabel.setText("Please select a patient to edit");
        }
    }

    /**
     * Displays the delete patient box.
     * @param actionEvent when pressing the delete patient icon, or the buttons is the
     *                    in "file" in the menubar-
     */
    @FXML
    private void displayDeletePatientBox(ActionEvent actionEvent) {
        Patient selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
        if(selectedPatient != null) {
            // Creates a new confirmation box
            Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
            confirmation.setTitle("Patient Register - Delete patient");
            confirmation.setHeaderText("Are you sure you want to delete this patient?");
            confirmation.setContentText(selectedPatient.toText());

            Optional<ButtonType> result = confirmation.showAndWait();
            if(result.get() == ButtonType.OK) {
                removePatient(selectedPatient);
            } else {
                System.out.println("Deletion cancelled");
            }
        } else {
            statusLabel.setText("Please select a patient to delete");
        }
    }

    /**
     * Displays the about box.
     * @param actionEvent when pressing the about button in "help" in the menubar
     */
    @FXML
    private void displayAboutBox(ActionEvent actionEvent) {
        Alert about = new Alert(Alert.AlertType.INFORMATION);
        about.setTitle("Patient Register - About");
        about.setHeaderText("Patient Register\n" +
                "v0.1-SNAPSHOT");
        about.setContentText("A register for patients\n" +
                "(C) Nicolay Roness\n" +
                "05.05.2021");
        about.showAndWait();
    }


    /**
     * Takes a file, if it is a .csv-file it will read it and import the data in to the table.
     */
    @FXML
    private void importFromCSV() {
        try {
            ArrayList<Patient> newPatients = PatientDataReader.readCSV();
            addAllPatients(newPatients);
        } catch (IOException e) {
            displayWrongFileTypeBox();
        }
    }

    /**
     * Exports the current data in the application to a .csv-file that the user then can give a name to and
     * choose a location for.
     *
     * !! NOTE FOR STUDASS !!
     * In the task description for task 4.2, it states:
     *
     * "Dersom det er en eksisterende fil med samme navn, skal dere vise en varsel som
     * viser melding “A file with same name exists. Do you want to overwrite it?”. Med å trykke på
     * “Yes” skal dere fjerne tidligere fil og lage en fil med samme navn. Når en velger “No”, skal
     * dere be om et nytt fil navn."
     *
     * This functionality is already built into the method #showSaveDialog in FileChooser, and therefore
     * I see no reason to add this functionality manually.
     *
     * @throws FileNotFoundException if there is a problem with the file.
     */
    @FXML
    private void saveToCSV() throws FileNotFoundException {
        PatientDataReader.saveToCSV(patients.getPatients());
    }

    /**
     * Adds new patients to the table.
     * @param patient the patient to be added.
     */
    public void newPatient(Patient patient) {
        addPatient(patient);
    }

    /**
     * Swaps an old patient with a new patient. This allows for simple editing of patients in the list.
     * @param oldPatient old/unedited patient.
     * @param newPatient new/edited patient.
     */
    public void editPatient(Patient oldPatient, Patient newPatient) {
        removePatient(oldPatient);
        addPatient(newPatient);
    }

    /**
     * Adds patient to both an instance of a PatientRegister as well as the observable list.
     * @param patient a new patient
     */
    private void addPatient(Patient patient) {
        patients.getPatients().add(patient);
        patientObservableList.add(patient);
    }

    /**
     * Removes patient from both an instance of a PatientRegister as well as the observable list.
     * @param patient patient to be removed
     */
    private void removePatient(Patient patient) {
        patients.getPatients().remove(patient);
        patientObservableList.remove(patient);
    }

    /**
     * Adds a list of patients from both an instance of a PatientRegister as well as the observable list.
     * @param newPatients ArrayList of patients.
     */
    private void addAllPatients(ArrayList<Patient> newPatients) {
        patients.getPatients().addAll(newPatients);
        patientObservableList.addAll(newPatients);
    }

    /**
     * Displays warning-alert. This method is called within the application if the user chooses something other
     * than a .csv-file.
     */
    private void displayWrongFileTypeBox() {
        Alert warning = new Alert(Alert.AlertType.WARNING);
        warning.setTitle("Wrong file type");
        warning.setHeaderText("The chosen file type is not valid");
        warning.setContentText("Please choose another file or cancel the operation");

        ButtonType buttonTypeSelectOtherFile = new ButtonType("Select another file");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        warning.getButtonTypes().setAll(buttonTypeSelectOtherFile, buttonTypeCancel);

        Optional<ButtonType> result = warning.showAndWait();
        if(result.get() == buttonTypeSelectOtherFile) {
            // The user gets to try again.
            importFromCSV();
        } else {
            warning.close();
        }
    }

    /**
     * Displays an .fxml-file with a title, and returns the FXMLLoader for further usage.
     * @param fxml filename of the .fxml-file that is to be displayed.
     * @param title the title of the stage.
     * @return the FXMLoader for further usage.
     * @throws IOException if the file can't be found.
     */
    private FXMLLoader displayPopup(String fxml, String title) throws IOException {
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
        Parent root = loader.load();

        // Sets the stage such that the rest of the application is inaccessible when this window is open.
        // Altso sets the title to the given title and the scene as the given .fxml-file.
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        stage.show();

        return loader;
    }





}
