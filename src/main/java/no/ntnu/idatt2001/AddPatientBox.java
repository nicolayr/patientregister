package no.ntnu.idatt2001;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.idatt2001.model.Patient;

public class AddPatientBox {

    @FXML
    private TextField firstNameInput;
    @FXML
    private TextField lastNameInput;
    @FXML
    private TextField ssnInput;
    @FXML
    private Label warningLabel;

    private PrimaryController primaryController;

    /**
     * Sends data in the text-fields are sent back and the stage closes.
     * @param actionEvent when the "OK" is pressed.
     */
    public void addNewPatientOk(ActionEvent actionEvent) {
        // Getting the user input
        String firstName = firstNameInput.getText();
        String lastName = lastNameInput.getText();
        String socialSecurityNumber = ssnInput.getText();

        // Sending the information back
        try {
            Patient newPatient = new Patient(socialSecurityNumber, firstName, lastName, "", "");
            primaryController.newPatient(newPatient);
            closeStage(actionEvent);
        } catch (IllegalArgumentException e) {
            warningLabel.setVisible(true);
        }
    }

    /**
     * If the cancel button is pressed, this method will be called. All it does is close the stage.
     * @param actionEvent cancel button is pressed.
     */
    public void addNewPatientCancel(ActionEvent actionEvent) {
        closeStage(actionEvent);
    }

    /**
     * Used in other classes to properly acces the needed methods
     * @param primaryController the primary controller of the application
     */
    public void setParentController(PrimaryController primaryController) {
        this.primaryController = primaryController;
    }

    /**
     * Closes the stage of an event upon the event.
     * @param actionEvent an event, for example a button being pressed.
     */
    private void closeStage(ActionEvent actionEvent) {
        Node n = (Node) actionEvent.getSource();
        Stage stage = (Stage) n.getScene().getWindow();
        stage.close();
    }

}
