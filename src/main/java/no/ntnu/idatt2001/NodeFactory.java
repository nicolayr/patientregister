package no.ntnu.idatt2001;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Class using the Factory design pattern for creating instances of JavaFX-nodes
 * This class is not actually used in the application, but is here to meet the requirement of showing the ability
 * to use this.
 */
public class NodeFactory {

    /**
     * Return a new Node-object based on the string in the parameter.
     * For example: if the paramter is "AnchorPane", this methods returns a new instance of AnchorPane.
     * @param nameOfNode JavaFX name of node to create. Not case-sensitive.
     * @return new instance of the requested Node. Null if the parameter does not match any of the Nodes implemented.
     */
    public Node createNode(String nameOfNode) {
        if(nameOfNode.equalsIgnoreCase("AnchorPane")) {
            return new AnchorPane();
        }
        else if(nameOfNode.equalsIgnoreCase("BorderPane")) {
            return new BorderPane();
        }
        else if(nameOfNode.equalsIgnoreCase("MenuBar")) {
            return new MenuBar();
        }
        else if(nameOfNode.equalsIgnoreCase("VBox")) {
            return new VBox();
        }
        else if(nameOfNode.equalsIgnoreCase("HBox")) {
            return new HBox();
        }
        else if(nameOfNode.equalsIgnoreCase("TableView")) {
            return new TableView<>();
        }
        else if(nameOfNode.equalsIgnoreCase("ImageView")) {
            return new ImageView();
        }
        else if(nameOfNode.equalsIgnoreCase("Button")) {
            return new Button();
        }
        else if(nameOfNode.equalsIgnoreCase("Label")) {
            return new Label();
        }
        else {
            return null;
        }
    }

}
