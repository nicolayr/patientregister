package no.ntnu.idatt2001.model;

import no.ntnu.idatt2001.model.Patient;

import java.util.ArrayList;

/**
 * Represents a list of patients.
 */
public class PatientRegister {

    private final ArrayList<Patient> patients;

    /**
     * Creates a list of patients
     */
    public PatientRegister() {
        this.patients = new ArrayList<>();
    }

    /**
     * Getter
     * @return the list of patients.
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds some testdata to the register.
     */
    private void fillRegister() {
        patients.add(new Patient(
                "123123", "Nicolay", "Roness", "", ""));
        patients.add(new Patient(
                "123123", "Test", "Testesen", "", ""));
    }

}
