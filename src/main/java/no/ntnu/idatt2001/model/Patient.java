package no.ntnu.idatt2001.model;

/**
 * Represents a patient at a hospital. Used for storing all needed information about a patient.
 */
public class Patient {

    // Object-variables
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor for a patient.
     * @param socialSecurityNumber the patient's social security number
     * @param firstName first name
     * @param lastName last name
     * @param diagnosis diagnosis of patient
     * @param generalPractitioner the patient general practitioner
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName,
                   String diagnosis, String generalPractitioner) throws IllegalArgumentException{
        try {
            Long.parseLong(socialSecurityNumber);
            if(socialSecurityNumber.length() == 11) {
                this.socialSecurityNumber = socialSecurityNumber;
            } else {
                throw new IllegalArgumentException("Social security number has to be eleven digits");
            }
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Social security number has to be a number");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    // Getters and setters
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        try {
            Long.parseLong(socialSecurityNumber);
            if(socialSecurityNumber.length() == 11) {
                this.socialSecurityNumber = socialSecurityNumber;
            } else {
                throw new IllegalArgumentException("Social security number has to be eleven digits");
            }
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Social security number has to be a number");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }

    /**
     * Return a readable version of a patient.
     * @return patient in a formatted string.
     */
    public String toText() {
        return firstName + " " + lastName + " - " + socialSecurityNumber;
    }

}
