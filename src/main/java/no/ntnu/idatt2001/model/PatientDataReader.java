package no.ntnu.idatt2001.model;

import javafx.stage.FileChooser;

import java.io.*;
import java.util.ArrayList;

/**
 * Class with methods reading and creating csv-files.
 */
public class PatientDataReader {

    /**
     * Reads a .csv-file of patient data, and creates an ArrayList of Patients based on that data.
     *
     * @throws IOException if there is a problem reading the given file, or if the file is not of the type CSV.
     */
    public static ArrayList<Patient> readCSV() throws IOException {
        File file = chooseFileToOpen();
        if(isCSV(file)) {
            BufferedReader csvReader = new BufferedReader(new FileReader(file));
            String row;
            ArrayList<Patient> patients = new ArrayList<>();
            if(csvReader.readLine() != null) { // Removes the first line, which contains unwanted data.
                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(";");
                    // We create an array of all the values in a row. These can now easily be accessed.
                    try {
                        patients.add(new Patient(data[3], data[0], data[1], "", data[2]));
                    } catch (IllegalArgumentException ignored) {
                        // When importing from .csv, if ssn is not valid, the patient will not be added.
                    }
                    // Adds new patients with the given data from each row to a list.
                }
            }
            csvReader.close();
            // This list is then returned.
            return patients;
        }
        throw new IOException();
    }

    /**
     * Takes an ArrayList of patients and creates a .csv/file with the given data. The user gets to choose the name
     * and the location of the file.
     *
     * @param patients an ArrayList of patients that is to be exported to a .csv-file.
     * @throws FileNotFoundException if there is a problem finding the file.
     */
    public static void saveToCSV(ArrayList<Patient> patients) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(chooseFileToSave());
        out.println("firstName;lastName;generalPractitioner;socialSecurityNumber;diagnosis");
        for(Patient p : patients) {
            out.println(p.getFirstName() + ";" + p.getLastName() + ";" + p.getGeneralPractitioner()
                    + ";" + p.getSocialSecurityNumber() + ";" + p.getDiagnosis());
        }
        out.close();
    }

    /**
     * Opens up a file-chooser, such that the user can send a file into the program.
     *
     * @return the chosen file
     */
    private static File chooseFileToOpen() {
        // User chooses file to import
        FileChooser chooser = new FileChooser();
        return chooser.showOpenDialog(null);
    }

    /**
     * Opens up a window where the user can choose a location and name for a .csv-file.
     *
     * @return a .csv-file which is ready to be added data into.
     */
    private static File chooseFileToSave() {
        FileChooser chooser = new FileChooser();

        // Makes it so the filetype is set to .csv
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV file (*.csv)", "*.csv");
        chooser.getExtensionFilters().add(extFilter);

        return chooser.showSaveDialog(null);
    }

    /**
     * Checks if the given file is a .csv-file (Comma separated values file).
     *
     * @param file file to be checked
     * @return true if file is of type .csv, false if not. The method also returns false if no
     *         file is chosen.
     */
    private static boolean isCSV(File file) {
        if(file != null) {
            String[] splitFile = file.getName().split("\\.");
            return splitFile[1].equalsIgnoreCase("csv");
        }
        return false;
    }


}

